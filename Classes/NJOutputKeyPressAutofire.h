//
//  NJOutputKeyPressAutofire.h
//  Enjoyable
//
//  Created by Grzegorz Moscichowski on 22/12/2016.
//
//

#import "NJOutputKeyPress.h"

@interface NJOutputKeyPressAutofire : NJOutputKeyPress
@property (nonatomic, assign) BOOL autofire;

@end
