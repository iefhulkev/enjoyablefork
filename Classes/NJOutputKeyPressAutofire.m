//
//  NJOutputKeyPressAutofire.m
//  Enjoyable
//
//  Created by Grzegorz Moscichowski on 22/12/2016.
//
//

#import "NJOutputKeyPressAutofire.h"
#import "NJKeyInputField.h"

@interface NJOutputKeyPressAutofire ()

@property (nonatomic) BOOL keyDown;
@property (nonatomic) NSDate *lastStateChange;

@end

@implementation NJOutputKeyPressAutofire

- (NSDictionary *)serialize {
    return self.keyCode != NJKeyInputFieldEmpty
    ? @{ @"type": self.class.serializationCode, @"key": @(self.keyCode),
         @"autofire": @(self.autofire)}
    : nil;
}

+ (NJOutput *)outputWithSerialization:(NSDictionary *)serialization {
    NJOutputKeyPressAutofire *output = [[NJOutputKeyPressAutofire alloc] init];
    output.keyCode = (short)[serialization[@"key"] integerValue];
    output.autofire = [serialization[@"autofire"] boolValue];
    return output;
}

- (void)_autofire {
    if (self.keyDown) {
        [self untrigger];
    } else {
        [self trigger];
    }
}

- (void)trigger {
    if (self.keyDown) return;
    self.keyDown = YES;
    [super trigger];
    self.lastStateChange = [NSDate date];
}

- (void)untrigger {
    if (!self.keyDown) return;
    self.keyDown = NO;
    [super untrigger];
    self.lastStateChange = [NSDate date];
}

- (BOOL)isContinuous {
    return self.autofire;
}

- (BOOL)update:(NJInputController *)ic {
    if (self.magnitude < 0.05) return NO; // dead zone
    NSDate *currentTime = [NSDate date];
    if ([currentTime timeIntervalSinceDate:self.lastStateChange] > 0.5 / self._frequency) {
        [self _autofire];
    }
    return YES;
}

- (double)_frequency {
    return self.magnitude * 10.0 + 10.0;
}

@end
